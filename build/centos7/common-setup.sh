#!/usr/bin/env bash
# Syntax: ./redhat-common.sh <install zsh flag> <username> <user UID> <user GID>

INSTALL_ZSH=$1
USERNAME=$2
USER_UID=$3
USER_GID=$4

set -e

if [ "$(id -u)" -ne 0 ]; then
    echo 'Script must be run a root. Use sudo or set "USER root" before running the script.'
    exit 1
fi

# Update to latest versions of packages
yum upgrade -y

# Install common dependencies
yum install -y \
    less \
    net-tools \
    which \
    curl \
    wget \
    procps \
    unzip \
    ca-certificates \
    openssl-libs \
    krb5-libs \
    libicu \
    autoconf \
    automake \
    libtool \
    make \
    zlib \
    libasan \
    gcc-c++ \
    libasan5 \
    devtoolset-8-gcc-c++ \
    devtoolset-8-libasan-devel \
    rh-python38-python-jinja2 \
    rh-python38-python-devel \
    rh-python38-python-pip\
    net-tools 

# install pipenv
source /opt/rh/rh-python38/enable
pip3 install pipenv

if [ "$USER_UID" = "" ]; then
    USER_UID=1000
fi

if [ "$USER_GID" = "" ]; then
    USER_GID=1000
fi

if [ "$USERNAME" = "" ]; then
    USERNAME=$(awk -v val=1000 -F ":" '$3==val{print $1}' /etc/passwd)
fi

if id -u $USERNAME > /dev/null 2>&1; then
    # User exists, update if needed
    if [ "$USER_GID" != "$(id -G $USERNAME)" ]; then
        groupmod --gid $USER_GID $USERNAME
        usermod --gid $USER_GID $USERNAME
    fi
    if [ "$USER_UID" != "$(id -u $USERNAME)" ]; then
        usermod --uid $USER_UID $USERNAME
    fi
else
    # Create user
    groupadd --gid $USER_GID $USERNAME
    useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME
    # setup user so it can check stuff out
    mkdir "/home/$USERNAME/.ssh"
    /bin/echo -e "Host git.vzbuilders.com\n\tStrictHostKeyChecking no\n" >> "/home/$USERNAME/.ssh/config"
    # just in case in old code is run using old git rrpo name
    /bin/echo -e "Host git.ouroath.com\n\tStrictHostKeyChecking no\n" >> "/home/$USERNAME/.ssh/config"
    chmod 600 "/home/$USERNAME/.ssh/config"
    chown $USER_UID:$USER_GID "/home/$USERNAME/.ssh/"
    chown $USER_UID:$USER_GID "/home/$USERNAME/.ssh/config"
fi

# Add add sudo support for non-root user
yum install -y sudo
echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME
chmod 0440 /etc/sudoers.d/$USERNAME

# Ensure ~/.local/bin is in the PATH for root and non-root users for bash
echo "export PATH=\$PATH:\$HOME/.local/bin" | tee -a /root/.bashrc >> /home/$USERNAME/.bashrc
chown $USER_UID:$USER_GID /home/$USERNAME/.bashrc

# Optionally install and configure zsh
if [ "$INSTALL_ZSH" = "true" ]; then
    yum install -y zsh
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    echo "export PATH=\$PATH:\$HOME/.local/bin" | tee -a /root/.zshrc
    cp -R /root/.oh-my-zsh /home/$USERNAME
    cp /root/.zshrc /home/$USERNAME
    sed -i -e "s/\/root\/.oh-my-zsh/\/home\/$USERNAME\/.oh-my-zsh/g" /home/$USERNAME/.zshrc
    echo ". /opt/rh/rh-python38/enable ; . /opt/rh/devtoolset-8/enable ">>/home/$USERNAME/.zshrc
    chown -R $USER_UID:$USER_GID /home/$USERNAME/.oh-my-zsh /home/$USERNAME/.zshrc
fi

echo ". /opt/rh/rh-python38/enable ; . /opt/rh/devtoolset-8/enable ">>/home/$USERNAME/.bashrc
chown -R $USER_UID:$USER_GID /home/$USERNAME/.bashrc

# add newer git
GIT_VER=2.26.0
yum install -y openssl-devel libcurl-devel gettext-devel
wget https://github.com/git/git/archive/v$GIT_VER.tar.gz
tar -zxvf v$GIT_VER.tar.gz
cd git-$GIT_VER \
    && make clean \
    && make configure \
    && ./configure --prefix=/usr/local/git \
    && make install -j4
yum remove -y openssl-devel libcurl-devel gettext-devel
cd .. ; rm -rf git-$GIT_VER v$GIT_VER.tar.gz